<?php

define('BASE_ROOT', realpath($_SERVER['DOCUMENT_ROOT'] . '/..'));
define('APP_ROOT', BASE_ROOT . DIRECTORY_SEPARATOR . 'app');

header_remove();

use System\Bootstrap;
Bootstrap::InitializeComponent();
