<?php

namespace System
{
    use Closure;
    use System\Support\DB;
    use System\Http\Request;
    use System\Http\Response;
    use System\Http\Component;
    use System\Support\Mailer;
    use Illuminate\Container\Container;
    use Dotenv\Dotenv;
    use GuzzleHttp\Psr7\ServerRequest;
    use Jenssegers\Blade\Blade;
    use Symfony\Component\HttpFoundation\Session\Session;

    use Aura\Router\{
        Map,
        RouterContainer
    };

    class Bootstrap
    {
        public static function InitializeComponent()
        {
            (new static)();
        }

        public function __invoke()
        {
            try {
                $dotenv = Dotenv::createImmutable(realpath('../'));
                $dotenv->safeLoad();
                $container = Container::getInstance();
                $container->instance(DB::class, DB::connection());
                $router = new RouterContainer();
                $map = $router->getMap();
                static::parseRoutes($map);
                $request = ServerRequest::fromGlobals();
                $matcher = $router->getMatcher();

                /** @var \Aura\Router\Route */
                $route = $matcher->match($request);

                if (!$route) {
                    $response = new Response('', 404);
                } else {
                    switch ($route->failedRule) {
                        case 'Aura\Router\Rule\Allows':
                            $response = new Response('', 405);
                            break;
                        case 'Aura\Router\Rule\Accepts':
                            $response = new Response('', 406);
                            break;
                    }

                    $request = new Request(
                        $_GET,
                        $_POST,
                        $route->attributes,
                        $_COOKIE,
                        $_FILES,
                        $_SERVER
                    );

                    if ($route->namePrefix != 'api') {
                        $session = new Session();
                        $session->setName("session");
                        $session->start();
                        $request->setSession($session);
                        $container->instance(Session::class, $session);
                        write_session($session->getName(), $session->getId(), 31536000);
                    }

                    $container->instance(Request::class, $request);
                    $container->instance(Response::class, new Response());
                    $container->instance(Mailer::class, new Mailer());
                    $container->instance(Blade::class, new Blade(APP_ROOT . '/templates', BASE_ROOT . '/storage/viewcache'));

                    if (!isset($response)) {
                        $callable = $route->handler;
                        $response = new Response();

                        if ($callable instanceof Closure) {
                            $response = $container->call($callable, $route->attributes);
                        } else if (class_exists($callable)) {
                            $instance = $container->make($callable);

                            if ($instance instanceof Component) {
                                if ($_SERVER['REQUEST_METHOD'] != "HEAD" && $_SERVER['REQUEST_METHOD'] != "GET") {
                                    if (method_exists($instance, 'form')) {
                                        $response = $container->call([$instance, 'form'], $route->attributes);
                                    }
                                } else if ($_SERVER['REQUEST_METHOD'] == "GET") {
                                    if (method_exists($instance, 'render')) {
                                        $response = $container->call([$instance, 'render'], $route->attributes);
                                    }
                                } else {
                                    $response = new Response('', 405);
                                }
                            }
                        } else {
                            $response = new Response('', 404);
                        }

                        if (!($response instanceof \Symfony\Component\HttpFoundation\Response)) {
                            if (is_string($response)) {
                                $response = new Response($response, Response::HTTP_OK);
                            } else if (is_array($response) || is_object($response)) {
                                $response = json_encode($response);
                                $response = new Response($response, Response::HTTP_OK, ['content-type' => 'application/json; charset=utf-8']);
                            } else {
                                $response = new Response('', 204);
                            }
                        }
                    }
                }
            } catch (\Throwable $e) {
                error('bootstrap', $e->getMessage() . ' on line ' . $e->getLine() . ' in file ' . $e->getFile());
                $response = new Response('', 500);
            }

            $response->sendHeaders()->sendContent();
        }

        public static function parseRoutes(Map $route)
        {
            $route->attach('api', '/api', function ($route) {
                include APP_ROOT . "/routes/api.php";
            });

            include APP_ROOT . "/routes/web.php";
        }
    }
}
