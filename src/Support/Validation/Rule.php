<?php

namespace System\Support\Validation
{
    abstract class Rule
    {
        public abstract function passes(string $field, string $value, array $data = []): bool;

        public abstract function message(string $field): string;
    }
}
