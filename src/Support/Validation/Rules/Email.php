<?php

namespace System\Support\Validation\Rules
{
    use System\Support\Validation\Rule;

    class Email extends Rule
    {
        public function passes(string $field, string $value, array $data = []): bool
        {
            return filter_var($value, FILTER_VALIDATE_EMAIL, FILTER_FLAG_EMAIL_UNICODE) !== false;
        }

        public function message(string $field): string
        {
            return "$field must be an email address";
        }
    }
}
