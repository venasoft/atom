<?php

namespace System\Support\Validation\Rules
{
    use System\Support\Validation\Rule;

    class Min extends Rule
    {
        protected $min;
        protected $max;

        protected $type;

        public function __construct($min, $max)
        {
            $this->min = (int) $min;
            $this->max = (int) $max;
        }

        public function passes(string $field, string $value, array $data = []): bool
        {
            if (is_numeric($value)) {
                $this->type = 'numeric';
                return ($value >= $this->min) && ($value <= $this->max);
            }

            $this->type = 'string';
            return (mb_strlen($value) >= $this->min) && (mb_strlen($value) <= $this->max);
        }

        public function message(string $field): string
        {
            if ($this->type == 'numeric') {
                return "$field must have a value between {$this->min} and {$this->max}";
            }

            return "$field must have a length between {$this->min} and {$this->max} characters";
        }
    }
}
