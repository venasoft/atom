<?php

namespace System\Support\Validation\Rules
{
    use System\Support\Validation\Rule;

    class Required extends Rule
    {
        public function passes(string $field, string $value, array $data = []): bool
        {
            return !empty($value);
        }

        public function message(string $field): string
        {
            return "$field is required";
        }
    }
}