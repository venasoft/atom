<?php

namespace System\Support\Validation\Rules
{
    use System\Support\Validation\Rule;

    class Max extends Rule
    {
        protected $max;

        protected $type;

        public function __construct($max)
        {
            $this->max = (int) $max;
        }

        public function passes(string $field, string $value, array $data = []): bool
        {
            if (is_numeric($value)) {
                $this->type = 'numeric';
                return ($value <= $this->max);
            }

            $this->type = 'string';
            return mb_strlen($value) <= $this->max;
        }

        public function message(string $field): string
        {
            if ($this->type == 'numeric') {
                return "$field must be less than the value {$this->min}";
            }

            return "$field must have a length less than {$this->min} characters";
        }
    }
}
