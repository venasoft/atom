<?php

namespace System\Support\Validation\Rules
{
    use System\Support\Validation\Rule;
    use System\Support\Validation\Validator;

    class RequiredWith extends Rule
    {
        protected $fields;

        protected $type;

        public function __construct(...$fields)
        {
            $this->fields = $fields;
        }

        public function passes(string $field, string $value, array $data = []): bool
        {
            foreach($this->fields as $field) {
                if ($value === '' && $data[$field] !== '') {
                    return false;
                }
            }

            return true;
        }

        public function message(string $field): string
        {
            $fields = Validator::aliases($this->fields);
            return "$field is required with " . implode(", ", $fields);
        }
    }
}
