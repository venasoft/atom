<?php

namespace System\Support\Validation\Rules
{
    use System\Support\Validation\Rule;

    class Optional extends Rule
    {
        public function passes(string $field, string $value, array $data = []): bool
        {
            return true;
        }

        public function message(string $field): string
        {
            return "";
        }
    }
}