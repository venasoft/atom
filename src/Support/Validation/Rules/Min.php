<?php

namespace System\Support\Validation\Rules
{
    use System\Support\Validation\Rule;

    class Min extends Rule
    {
        protected $min;

        protected $type;

        public function __construct($min)
        {
            $this->min = (int) $min;
        }

        public function passes(string $field, string $value, array $data = []): bool
        {
            if (is_numeric($value)) {
                $this->type = 'numeric';
                return ($value >= $this->min);
            }

            $this->type = 'string';
            return mb_strlen($value) >= $this->min;
        }

        public function message(string $field): string
        {
            if ($this->type == 'numeric') {
                return "$field must be greater than the value {$this->min}";
            }

            return "$field must have a length greater than {$this->min} characters";
        }
    }
}
