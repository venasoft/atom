<?php

namespace System\Support\Validation
{
    use System\Support\Validation\Rules\{
        Required,
        Optional,
        Email,
        Max,
        Min,
        Between
    };

    class RulesMap
    {
        protected static $ruleMap = [
            'required' => Required::class,
            'required_with' => RequiredWith::class,
            'email' => Email::class,
            'max' => Max::class,
            'min' => Min::class,
            'between' => Between::class,
            'nullable' => Optional::class
        ];

        public static function hasRule(string $rule): bool
        {
            return isset(self::$ruleMap[$rule]);
        }

        public function resolveRule(string $rule, array $options = []): Rule
        {
            return new self::$ruleMap[$rule](...$options);
        }
    }
}
