<?php

namespace System\Support\Validation
{
    use System\Support\Validation\Rules\Optional;

    class Validator
    {
        protected $input;

        protected $rules;

        protected $errors;

        protected static $aliases;

        public function __construct(array $input)
        {
            $this->input = $this->extractWildcardInput($input);
            self::$aliases = [];
            $this->errors = new ErrorBag;
        }

        protected function extractWildcardInput(array $data, string $root = '', array $results = [])
        {
            foreach($data as $key => $value) {
                if (is_array($value)) {
                    $results = array_merge($results, $this->extractWildcardInput($value, $root . $key . '.'));
                } else {
                    $results[$root . $key] = $value;
                }
            }

            return $results;
        }

        public function setRules(array $rules): void
        {
            $this->rules = $rules;
        }

        public static function make(array $input, array $rules, array $aliases = []): self
        {
            $validator = new self($input);
            $validator->setRules($rules);
            $validator->setAliases($aliases);

            return $validator;
        }

        public function failed(): bool
        {
            $errors = $this->errors->all();
            return !empty($errors);
        }

        public function errors(?string $field = null): array
        {
            if ($field) {
                return $this->errors->getErrorsFor($field);
            }

            return $this->errors->all();
        }

        public function validate()
        {
            foreach($this->rules as $field => $rules) {
                $rules = $this->resolveRules($rules);

                foreach($rules as $rule) {
                    $this->validateRule($field, $rule, $this->resolvedRulesAreOptional($rules));
                }
            }
        }

        protected function resolvedRulesAreOptional(array $rules): bool
        {
            foreach($rules as $rule) {
                if ($rule instanceof Optional) {
                    return true;
                }
            }

            return false;
        }

        public function setAliases(array $aliases)
        {
            self::$aliases = $aliases;
        }

        public function alias(string $field): string
        {
            return self::$aliases[$field] ?? $field;
        }

        public static function aliases(array $fields)
        {
            return array_map(function($field) {
                return self::$aliases[$field] ?? $field;
            }, $fields);
        }

        protected function resolveRules(array $rules): array
        {
            $rules = array_map(function($rule) {
                if (is_string($rule)) {
                    return $this->getRule($rule);
                }
            }, $rules);

            return array_filter($rules);
        }

        protected function getRule(string $rule):?Rule
        {
            $data = explode(":", $rule, 2);

            if (RulesMap::hasRule($data[0])) {
                return RulesMap::resolveRule($data[0], explode(',', end($data)));
            }

            return null;
        }

        protected function validateRule(string $field, Rule $rule, bool $optional)
        {
            $data = $this->getMatchingInput($field);

            foreach ($data as $matchedField) {
                if ($optional && ($value = $this->getFieldValue($matchedField, $this->input)) === '') {
                    continue;
                }

                $this->validateUsingRuleObject($matchedField, $value, $rule);
            }
        }

        protected function validateUsingRuleObject(string $field, string $value, Rule $rule)
        {
            if (!$rule->passes($field, $value, $this->input)) {
                $this->errors->add($field, $rule->message(self::alias($field)));
            }
        }

        protected function getMatchingInput($field)
        {
            return preg_grep('/^' . str_replace('*', '([^\.]+)', $field) . '$/', array_keys($this->input));
        }

        protected function getFieldValue($field, $data)
        {
            $data[$field] ?? null;
        }
    }
}
