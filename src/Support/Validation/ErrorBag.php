<?php

namespace System\Support\Validation
{
    class ErrorBag
    {
        protected $errors;

        public function __construct()
        {
            $this->errors = [];
        }

        public function add(string $key, string $value)
        {
            $this->errors[$key][] = $value;
        }

        public function getErrorsFor(string $key): array
        {
            return $this->errors[$key] ?? [];
        }

        public function all()
        {
            return $this->errors;
        }
    }
}
