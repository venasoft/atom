<?php

namespace System\Support
{
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\SMTP;
    use PHPMailer\PHPMailer\Exception as MailException;

    class Mailer
    {
        private $to;
    
        private $cc;
    
        private $bcc;
    
        private $html;
    
        private $text;
    
        private $content;
    
    
        public function __construct()
        {
            $this->clear();
        }
    
        private function clear()
        {
            $this->to = [];
            $this->cc = [];
            $this->bcc = [];
            $this->html = '';
            $this->text = '';
            $this->subject = '';
        }
    
        public function toAddress(string $address, string $name = '')
        {
            $this->to[] = [$address, $name];
            return $this;
        }
    
        public function ccAddress(string $address, string $name = '')
        {
            $this->cc[] = [$address, $name];
            return $this;
        }
    
        public function bccAddress(string $address, string $name = '')
        {
            $this->bcc[] = [$address, $name];
            return $this;
        }
    
        public function html(string $content)
        {
            $this->html = $content;
            return $this;
        }
    
        public function text(string $content)
        {
            $this->text = $content;
            return $this;
        }
    
        public function subject(string $content)
        {
            $this->subject = $content;
            return $this;
        }
    
        public function send(): bool
        {
            $mail = new PHPMailer(true);

            try {
                $mailSecure = null;
    
                if ($_SERVER['MAIL_ENCRYPTION'] == 'tls') {
                    $mailSecure = PHPMailer::ENCRYPTION_STARTTLS;
                } else if ($_SERVER['MAIL_ENCRYPTION'] == 'ssl') {
                    $mailSecure = PHPMailer::ENCRYPTION_SMTPS;
                }
    
                $mail->SMTPDebug = SMTP::DEBUG_OFF;
                $mail->isSMTP();
                $mail->Host       = $_SERVER['MAIL_HOST'];
                $mail->SMTPAuth   = $_SERVER['MAIL_AUTH'];
                $mail->Username   = $_SERVER['MAIL_USERNAME'];
                $mail->Password   = $_SERVER['MAIL_PASSWORD'];
                $mail->SMTPSecure = $mailSecure;
                $mail->Port       = $_SERVER['MAIL_PORT'];
                $mail->XMailer    = "Venasoft Echo/4.1";
    
                $mail->setFrom($_SERVER['MAIL_FROM_ADDRESS'], $_SERVER['MAIL_FROM_NAME']);
                $mail->addReplyTo($_SERVER['MAIL_FROM_ADDRESS']);
    
                foreach($this->to as $recipient) {
                    $mail->addAddress($recipient[0], $recipient[1]);
                }
    
                foreach($this->cc as $recipient) {
                    $mail->addCC($recipient[0], $recipient[1]);
                }
    
                foreach($this->bcc as $recipient) {
                    $mail->addBCC($recipient[0], $recipient[1]);
                }
    
                $mail->Subject = $this->subject;
    
                if (!empty($this->html)) {
                    $mail->isHTML(true);
                    $mail->Body    = $this->html;
                    $mail->AltBody = $this->text;
                } else {
                    $mail->Body    = $this->text;
                }
    
                $mail->send();
                $this->clear();
                return true;
            } catch (MailException $e) {
                warning('mailer', "Message could not be sent. Mailer Error: {$mail->ErrorInfo}");
            }
    
            $this->clear();
            return false;
        }
    }
}
