<?php

namespace System\Support
{
    use Doctrine\DBAL\DriverManager;
    use Doctrine\DBAL\Query\QueryBuilder;

    class DB extends QueryBuilder
    {
        public static function connection(): ?DB
        {
            static $builder = null;

            if (is_null($builder)) {
                $conn = DriverManager::getConnection([
                    'dbname' => $_SERVER['DATABASE_SCHEMA'],
                    'user' => $_SERVER['DATABASE_USERNAME'],
                    'password' => $_SERVER['DATABASE_PASSWORD'],
                    'host' => $_SERVER['DATABASE_HOST'],
                    'port' => $_SERVER['DATABASE_PORT'],
                    'driver' => 'pdo_mysql',
                ]);

                $builder = new DB($conn);
            }

            return $builder;
        }
    }
}
