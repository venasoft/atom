<?php

use Jenssegers\Blade\Blade;
use System\Http\Request;
use System\Http\Response;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Illuminate\Container\Container;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use System\Support\DB;

function uuid(): string
{
    return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        random_int(0, 0xffff) , random_int(0, 0xffff),
        random_int(0, 0xffff),
        random_int(0, 0x0fff) | 0x4000,
        random_int(0, 0x3fff) | 0x8000,
        random_int(0, 0xffff), random_int(0, 0xffff), random_int(0, 0xffff)
    );
};

function write_session(string $name, string $value, int $expires = 0): void
{
    $request = request();
    $date = date("D, d-M-Y H:i:s", time() + $expires);
    $secure = "";

    if ($request->isSecure()) {
        $secure = "Secure; ";
    }

    header("set-cookie: {$name}={$value}; expires={$date}; Max-Age={$expires}; path=/; {$secure}HttpOnly");
}

function app(): Container
{
    return Container::getInstance();
}

function view(string $view, array $params = []): string
{
    return app()->make(Blade::class)->make($view, $params)->render();
}

function request(): Request
{
    return app()->make(Request::class);
}

function session(): SessionInterface
{
    return request()->getSession();
}

function db(): DB
{
    return app()->make(DB::class);
}

function response(string $content = '', int $status = 200, array $headers = []): Response
{
    return new Response($content, $status, $headers);
}

function error(string $name, string $message): void
{
    $log = new Logger($name);
    $log->pushHandler(new StreamHandler(BASE_ROOT . '/storage/logs/errors.log', Logger::WARNING));
    $log->error($message);
    $log->close();
}

function warning(string $name, string $message): void
{
    $log = new Logger($name);
    $log->pushHandler(new StreamHandler(BASE_ROOT . '/storage/logs/errors.log', Logger::WARNING));
    $log->warning($message);
    $log->close();
}
